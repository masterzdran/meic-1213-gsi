
-- ponto 0

set statistics IO on

select * from t where i = 15
select * from t1 where i = 15

-- ponto 1


CHECKPOINT
dbcc dropcleanbuffers

select * from t where i = 15
select * from t where i = 15

-- ponto 2

select * from t1 where i = 15
select * from t1 where i = 15

-- ponto 3