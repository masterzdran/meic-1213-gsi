drop table t
drop table t1

create table t (i int primary key, j char(5000))
create table t1 (i int, j char(5000))


set statistics IO Off


declare @i int
set @i = 1
while @i <= 20
begin

    insert into t values(@i,REPLICATE('a',5000))
    insert into t1 values(@i,REPLICATE('a',5000))
    set @i = @i+1
end
