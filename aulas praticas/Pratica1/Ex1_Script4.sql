drop table t
create table t (i int, j char(3000), k varchar(6000))




declare @i int
set @i = 1
while @i <= 20
begin

    insert into t values(@i,'A',replicate('B', 800))
    set @i = @i+1
end


-- ponto 1
-- ver estrutura do heap (anotar n� de p�gidas de dados)
DBCC IND ('GSI_Ap1','t',1)

-- ponto 2
-- ver % de espa�o usado por p�gina
SELECT avg_page_space_used_in_percent
FROM sys.dm_db_index_physical_stats(DB_ID(N'GSI_Ap1'),OBJECT_ID(N'T'),
                                    DEFAULT,DEFAULT,'DETAILED'
) WHERE index_level = 0;


--ponto 3
alter table t rebuild

-- ver novamente estrutura do heap (anotar n� de p�gidas de dados)
DBCC IND ('GSI_Ap1','t',1)

-- ponto 4
-- ver % de fragmenta��o (interna ou l�gica)
SELECT avg_page_space_used_in_percent
FROM sys.dm_db_index_physical_stats(DB_ID(N'GSI_Ap1'),OBJECT_ID(N'T'),
                                    DEFAULT,DEFAULT,'DETAILED'
) WHERE index_level = 0;



-- ponto 5
update t set k = replicate('C',6000)

-- ver novamente estrutura do heap (anotar n� de p�gidas de dados)
DBCC IND ('GSI_Ap1','t',1)


-- ponto 6
alter table t rebuild

-- ver novamente estrutura do heap (anotar n� de p�gidas de dados)
DBCC IND ('GSI_Ap1','t',1)


-- ponto 7
-- ver primeira p�gina de dados
DBCC TRACEON (3604);
DBCC PAGE ('GSI_Ap1',1,578,3)


