USE master;
GO
CREATE DATABASE GSI_AP1 
ON 
PRIMARY
( NAME = GSI_AP1_dat,
   FILENAME = 'C:\Users\fabio\Repos\meic-1213-gsi\aulas praticas\Pratica1\ap1.mdf',
   SIZE = 3MB,
   MAXSIZE = 3MB,
   FILEGROWTH = 0 )
LOG ON
( NAME = 'GSI_AP1_log',
   FILENAME = 'C:\Users\fabio\Repos\meic-1213-gsi\aulas praticas\Pratica1\ap1log.ldf',
   SIZE = 1MB,
   MAXSIZE = 1MB,
   FILEGROWTH = 0 )
GO