

--ponto 0
select i,k from t where  i between 100 and 200
select I,K from t where k = 200

--ponto 1
create index i1 on t(i,k)

select i,k from t where  i between 100 and 200

select i,j from t where  i between 100 and 200

select I,K from t where k = 200

select j from t where i = 200 and k = 200




--ponto 3
drop index i1 on t
create index i1 on t(i)include(k)

select i,k from t where  i between 100 and 200

select i,j from t where  i between 100 and 200

select I,K from t where k = 200

select j from t where i = 200 and k = 200