drop table t
drop table t1

create table t (i int, j varchar(2) check (j in ('O','A','B','AB')))
create table t1 (i int, j char(2) check (j in ('O','A','B','AB')))


set statistics IO Off


declare @i int
set @i = 1
while @i <= 2000
begin

    insert into t values(@i,'O')
    insert into t1 values(@i,'O')
    set @i = @i+1
end


-- ponto 1
dbcc checktable('t')


-- ponto 2
dbcc checktable('t1')