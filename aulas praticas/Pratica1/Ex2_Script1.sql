
drop table t
drop table t1
create table t (i int primary key, j char(1000))

create table t1 (i int primary key, j char(1000))

set statistics IO off

declare @i int
set @i = 1
while @i <= 20
begin

    insert into t values(@i*2,CAST(@i*2 as CHAR(1000)))
    insert into t1 values(@i*2,CAST(@i*2 as CHAR(1000)))
    set @i = @i+1
end

