-- ponto 0: verificar estruturas dos clustered indexes
DBCC IND ('GSI_Ap1','t',1)
DBCC IND ('GSI_Ap1','t1',1)


-- ponto 1 -- verificar estruturas das primeiras p�ginas das folhas
SELECT sys.fn_PhysLocFormatter(%%physloc%%), * FROM t where i BETWEEN 2 AND 20
SELECT sys.fn_PhysLocFormatter(%%physloc%%), * FROM t1 where i BETWEEN 2 AND 20


-- ponto 2 -- verificar n.� de logical reads
set statistics io on
insert into t values(3,'A')
insert into t1 values(3,'A')
set statistics io off

-- ponto 3: verificar novas estruturas dos clustered indexes
DBCC IND ('GSI_Ap1','t',1)
DBCC IND ('GSI_Ap1','t1',1)

-- ponto 4 -- verificar estruturas das primeiras p�ginas das folhas
SELECT sys.fn_PhysLocFormatter(%%physloc%%), * FROM t where i BETWEEN 2 AND 20
SELECT sys.fn_PhysLocFormatter(%%physloc%%), * FROM t1 where i BETWEEN 2 AND 20

-- ponto 5: verificar estrutura dos clustered indexes
insert into t values(15,'B')